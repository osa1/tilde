function checkAdj(rect, px)
    if rect.x - 1 <= px.x and rect.x + rect.width + 1 >= px.x and
            rect.y - 1 <= px.y and rect.y + rect.height + 1 >= px.y then
        return true
    end
    return false
end

function findAdjRects(rects, px)
    local r = {}
    for i, rect in ipairs(rects) do
        if checkAdj(rect, px) then r[#r+1] = i end
    end
    return r
end

function extendRect(rect, px)
    local top    = math.min(rect.y, px.y)
    local left   = math.min(rect.x, px.x)
    local bottom = math.max(rect.y+rect.height, px.y+1)
    local right  = math.max(rect.x+rect.width, px.x+1)

    rect.x = left
    rect.y = top
    rect.width  = right  - left
    rect.height = bottom - top
end

function mergeRects(rect1, rect2)
    local top    = math.min(rect1.y, rect2.y)
    local bottom = math.max(rect1.y + rect1.height, rect2.y + rect2.height)
    local left   = math.min(rect1.x, rect2.x)
    local right  = math.max(rect1.x + rect1.width, rect2.x + rect2.width)

    return { x = left, y = top, width = right - left, height = bottom - top }
end
