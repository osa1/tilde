require "sprite"
require "rect"

tilde = {}

function tilde.new(imagePath)
    local imageData = love.image.newImageData(imagePath)
    local image     = love.graphics.newImage(imageData)

    local marginX, marginY = 50, 50

    local screenWidth, screenHeight = imageData:getWidth(), imageData:getHeight()
    love.graphics.setMode(screenWidth+marginX*2, screenHeight+marginY*2, false, false, 0)

    local groups = findSpriteGroups(imageData, {r=255, g=255, b=255}) -- TODO: fix r,g,b part
    for i=1, #groups do
        groups[i].name = "sprite"..i
    end

    local dynamicPanel = loveframes.Create("panel")
    dynamicPanel:SetVisible(false)
    dynamicPanel:SetSize(150, 20)

    local staticPanel = loveframes.Create("panel")
    staticPanel:SetVisible(false)
    staticPanel:SetSize(150, 20)

    local staticPanelText = loveframes.Create("text")
    staticPanelText:SetParent(staticPanel)

    local dynamicPanelText = loveframes.Create("text")
    dynamicPanelText:SetParent(dynamicPanel)

    local selectedGroup = nil

    tilde.imageData = imageData
    tilde.image = image
    tilde.marginX = marginX
    tilde.marginY = marginY
    tilde.screenWidth = screenWidth
    tilde.screenHeight = screenHeight
    tilde.groups = groups
    tilde.dynamicPanel = dynamicPanel
    tilde.staticPanel = staticPanel
    tilde.dynamicPanelText = dynamicPanelText
    tilde.staticPanelText = staticPanelText
    tilde.selectedGroup = selectedGroup
end

function tilde.update(dt)
    loveframes.update(dt)
end

function tilde.draw()
    love.graphics.translate(tilde.marginX, tilde.marginY)
    love.graphics.draw(tilde.image, 0, 0)

    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(255, 255, 255, 100)
    for _, gr in ipairs(tilde.groups) do
        if gr.selected then love.graphics.setColor(255, 255, 255, 200) end
        love.graphics.rectangle("line", gr.x, gr.y, gr.width, gr.height)
        if gr.selected then love.graphics.setColor(255, 255, 255, 100) end
    end
    love.graphics.setColor(r, g, b, a)

    local mouseX, mouseY = love.mouse.getPosition()
    for i, g in ipairs(tilde.groups) do
        if checkAdj({ x = g.x + tilde.marginX, y = g.y + tilde.marginY, width = g.width, height = g.height},
                    { x = mouseX, y = mouseY }) then
            tilde.dynamicPanel:SetPos(g.x, g.y)
            tilde.dynamicPanelText:SetText(g.name.."{".. table.concat({g.x, g.y, g.width, g.height}, ",") .. "}")
            tilde.dynamicPanel:SetVisible(true)
            break
        end
    end

    loveframes.draw()
    love.graphics.setCaption(love.timer.getFPS())
end

function tilde.setSelected(g)
    tilde.staticPanel:SetPos(g.x, g.y)
    tilde.staticPanelText:SetText(g.name.."{".. table.concat({g.x, g.y, g.width, g.height}, ",") .. "}")
    tilde.staticPanelText:SetVisible(true)
    tilde.staticPanel:SetVisible(true)
    tilde.selectedGroup = g
    g.selected = true
end

function tilde.resetSelected()
    for _, g in ipairs(tilde.groups) do
        g.selected = false
    end
end

function tilde.mousepressed(x, y, button)
    for _, g in ipairs(tilde.groups) do
        if checkAdj({ x = g.x + tilde.marginX, y = g.y + tilde.marginY, width = g.width, height = g.height},
                    { x = x, y = y }) then
            if tilde.selectedGroup then
                if tilde.selectedGroup == g then
                    tilde.selectedGroup = nil
                    tilde.staticPanel:SetVisible(false)
                    g.selected = false
                else
                    tilde.resetSelected()
                    tilde.setSelected(g)
                end
            else
                tilde.setSelected(g)
            end
            break
        end
    end

    loveframes.mousepressed(x, y, button)
end

function tilde.mousereleased(x, y, button)
    loveframes.mousereleased(x, y, button)
end

function tilde.keypressed(key, unicode)
    if key == "escape" then love.event.push("quit") end

    loveframes.keypressed(key, unicode)
end

function tilde.keyreleased(key, unicode)
    loveframes.keyreleased(key, unicode)
end
