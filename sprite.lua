function findSpriteGroups(imageData, key)
    local groups = {}

    for y=0, imageData:getHeight()-1 do
        for x=0, imageData:getWidth()-1 do
            local r, g, b, a = imageData:getPixel(x, y)

            if a ~= 0 then
                local adjGroups = findAdjRects(groups, {x=x, y=y})
                table.sort(adjGroups)

                if #adjGroups ~= 0 then
                    extendRect(groups[adjGroups[1]], {x=x, y=y})
                    if #adjGroups > 1 then
                        for i=#adjGroups, 2, -1 do
                            groups[adjGroups[1]] = mergeRects(groups[adjGroups[1]], groups[adjGroups[i]])
                            table.remove(groups, adjGroups[i])
                        end
                    end
                else
                    groups[#groups+1] = { x=x, y=y, width=1, height=1 }
                end

            end

        end
    end

    return groups
end
